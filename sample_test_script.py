#!/usr/bin/env python

import unittest

class SomeUnitTest(unittest.TestCase):
    def test_sanity(self):
        self.assertEqual(True, True)
        
    def test_False(self):
        self.assertTrue(False is not True)
        
    def test_True(self):
        self.assertTrue(True is True)


if __name__ == '__main__':
    unittest.main()